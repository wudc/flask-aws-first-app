from flask import Flask
from flask_sqlalchemy import SQLAlchemy

application = Flask(__name__)             # create an application instance
# use secret key to prevent vulnerbility such as cross-site script. Key is generated via Python command
# import secrets
#   secrets.token_hex(16) - 16 characters. More if needed
application.config['SECRET_KEY'] = '6721497159e6650c8feade7a2307b775'
application.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///site.db'
db=SQLAlchemy(application)

from flask_blog import controller