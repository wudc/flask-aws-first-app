from flask_blog import application
if __name__ == "__main__":        # on running python application.py
    application.run(debug=True)           # run the flask application in debug mode